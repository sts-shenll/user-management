<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

/**
 * Login Controller
 *
 *
 * @method \App\Model\Entity\Login[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LoginController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->users = TableRegistry::get('Users');
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(array('forget'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $requestData = $this->request->getData();
        $session = $this->request->getSession();
        $state = "1";
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $state = "1";
                $this->Auth->setUser($user);
                $session->write('name', $user['name']);
                if ($user["role"] == "admin") {
                    return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                } else {
                    return $this->redirect(['controller' => 'Login', 'action' => 'index']);
                }
            } else {
                $state = "0";
            }
        }
        $session->destroy();
        $this->set(compact('requestData', 'session', 'state'));
        $this->viewBuilder()->setLayout('login');
        $this->render('index');
    }

    public function logout()
    {
        $session = $this->getRequest()->getSession();
        $session->destroy();
        return $this->redirect($this->Auth->logout());
    }
}
