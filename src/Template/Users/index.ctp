<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="col-md-12 col-sm-12 col-xs-12 total-container">
  <div class="col-md-2 col-sm-4 col-xs-12 float-left total-container">
    <nav class="navbar-container">
        <ul class="side-nav">
            <li class="heading"><?= __('Actions') ?></li>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?><i></i></li>
        </ul>
    </nav>
  </div>
  <div class="col-md-9 col-sm-8 col-xs-12 float-left">
    <div class="col-12 title-container">
      <h3><?= __('Users') ?></h3>
    </div>
    <div class="table-container">
      <div class="col-md-12 table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" class="table_head"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col" class="table_head"><?= $this->Paginator->sort('email') ?></th>
                    <th scope="col" class="table_head"><?= $this->Paginator->sort('phone_number') ?></th>
                    <th scope="col" class="table_head"><?= $this->Paginator->sort('date_of_birth') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= h($user->name) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user->phone_number) ?></td>
                    <td><?= h($user->date_of_birth) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
      </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
  </div>
</div>

  <div class="container-fluid">

  </div>
