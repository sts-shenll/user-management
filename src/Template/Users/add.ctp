<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="col-md-12 col-sm-12 col-xs-12 total-container">
  <div class="col-md-2 col-sm-4 col-xs-12 float-left total-container">
    <nav class="navbar-container">
        <ul class="side-nav">
            <li class="heading"><?= __('Actions') ?></li>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?><i></i></li>
            <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?><i></i></li>
        </ul>
    </nav>
  </div>
  <div class="col-md-9 col-sm-8 col-xs-12 float-left">
    <?= $this->Form->create($user, ['id' => 'add_user_form']) ?>
      <fieldset class="form-container">
          <legend class="add-user-title"><?= __('Add User') ?></legend>
          <?php
              echo $this->Form->control('name');
              echo $this->Form->control('email');
              echo $this->Form->control('phone_number');
          ?>
          <div class="input">
            <label for="date_of_birth">Date of Birth</label>
            <input name="date_of_birth" data-date-format="dd/mm/yyyy" class="datepicker" id="datetimepicker1" />
          </div>
      </fieldset>
      <button type="submit" class="submit-button">Submit</button>
    <?= $this->Form->end(); ?>
  </div>
</div>
