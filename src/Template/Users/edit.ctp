<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="col-md-12 col-sm-12 col-xs-12 total-container">
  <div class="col-md-2 col-sm-4 col-xs-12 float-left total-container">
    <nav class="navbar-container">
      <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?><i></i></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?><i></i></li>
      </ul>
    </nav>
  </div>
  <div class="col-md-9 col-sm-8 col-xs-12 float-left">
    <?= $this->Form->create($user, ['id' => 'add_user_form']) ?>
    <fieldset class="form-container">
        <legend class="add-user-title"><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('phone_number');
        ?>
        <div class="input">
          <label for="date_of_birth">Date of Birth</label>
          <input name="date_of_birth" data-date-format="dd/mm/yyyy" class="datepicker" id="datetimepicker1" <?= date("d/m/Y",strtotime($user->date_of_birth)); ?> value="<?= date("d/m/Y",strtotime($user->date_of_birth)); ?>"/>
        </div>
    </fieldset>
    <button type="submit" class="submit-button">Submit</button>
    <?= $this->Form->end() ?>
  </div>
</div>
