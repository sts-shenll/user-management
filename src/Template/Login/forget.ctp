<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<h3 class="login-title"><?= $settingsData['long_title']; ?></h3>
<?php
    if($show_security_question == "no") {?>
<?= $this->Form->create('',['class' => 'form-horizontal login-form']); ?>
<h3>Reset Password</h3>
<?php
    if($state == "0") {?>
        <div class="alert alert-danger alert-dismissible">
            <button class="close" data-close="alert"></button><span><?= $msg; ?></span>
        </div>
    <?php }
 ?>
<div class="form-body">
    <div class="form-group">
        <label class="control-label col-md-4">PAO Code</label>
        <div class="col-md-8">
            <input type="text" name="pao_code" value="<?= isset($requestData['pao_code']) ? $requestData['pao_code'] : ''; ?>" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4">DDO Code</label>
        <div class="col-md-8">
            <input type="text" name="ddo_code" value="<?= isset($requestData['ddo_code']) ? $requestData['ddo_code'] : ''; ?>" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4">Username</label>
        <div class="col-md-8">
            <input type="text" name="username" value="<?= isset($requestData['username']) ? $requestData['username'] : ''; ?>" class="form-control">
        </div>
    </div>
</div>
<div class="form-actions">
    <button type="submit" class="btn green">submit</button>
        <a href="/" class="btn btn-primary btn-go-back"> << Go Back</a>
</div>
<?= $this->Form->end(); ?>
<?php } else if($show_security_question == "yes") { ?>
<?= $this->Form->create('',['id'=>'forgot_password_form', 'class' => 'form-horizontal forget-form']); ?>
    <h3>Reset Password</h3>
    <?php
        if($state == "0") {?>
            <div class="alert alert-danger alert-dismissible">
                <button class="close" data-close="alert"></button><span><?= $msg; ?></span>
            </div>
        <?php }
     ?>
    <div class="form-body ">
        <div class="form-group">
            <label class="control-label col-md-4">Security question</label>
            <div class="col-md-8">
                <label class="control-label security-question"><?= $security_question; ?></label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4">Answer</label>
            <div class="col-md-8">
                <input type="text" class="form-control" name="security_answer" placeholder="Answer"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4">Password</label>
            <div class="col-md-8">
                <input name="password" type="password" class="form-control" id="password" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4">Confirm Password</label>
            <div class="col-md-8">
                <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password">
            </div>
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green">Reset my password</button>
        <a href="/forget-password" class="btn btn-primary btn-go-back"> << Go Back</a>
    </div>
<?= $this->Form->end(); ?>
<?php } else if($success_screen === "yes") { ?>
    <form class="form-horizontal forget-form">
        <div class="alert alert-success alert-dismissible">
            <span><?= $msg; ?></span>
        </div>
        <div class="form-actions">
            <a href="/" class="btn btn-primary btn-go-back"> << Click here to login</a>
        </div>
    </form>
<?php } ?>
<?php
    if($scrollMsg === true) {?>
        <marquee direction="left">
            <?php echo $settingsData['scroll_msg']; ?>
        </marquee>
<?php } ?>
<?php if($cookiebar != 'hide') { ?>
<div class="mt-cookie-consent-bar">
    <div class="container">
        <div class="mt-cookie-consent-bar-holder">
            <div class="mt-cookie-consent-bar-content"> This website uses cookies to ensure you get the best experience on our website.
                <a href="javascript:;" tatget="_blank">What is cookie ?</a>
                <a href="javascript:;">Our Cookie Policy</a>
            </div>
            <div class="mt-cookie-consent-bar-action">
                <a href="javascript:;" class="mt-cookie-consent-btn btn btn-circle green">Understand</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
