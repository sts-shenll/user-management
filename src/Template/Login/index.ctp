<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="login-page">
  <div class="col-md-12 col-sm-12">
    <div class="col-md-4">
    </div>
    <div class="col-md-4 login-container">
      <?= $this->Form->create('',['class' => 'form-horizontal login-form']); ?>
          <h3 class="form-title text-center login-title login-text">Login</h3>
          <?php if($state == "0") {?>
             <div class="alert alert-danger alert-dismissible">
                  <button class="close" data-close="alert"></button><span> Invalid Email/Password.</span>
              </div>
          <?php } ?>
          <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9 login-text">Email</label>
              <div class="input-icon">
                  <i class="fa fa-user"></i>
                  <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="email" />
              </div>
          </div>
          <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9 login-text">Password</label>
              <div class="input-icon">
                  <i class="fa fa-lock"></i>
                  <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                  placeholder="Password" name="password" /> </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
              <button type="submit" class="btn green customlogin"> <i class="fa fa-sign-in"></i> Login </button>
            </div>
          </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="col-md-4">
    </div>
  </div>
</div>
